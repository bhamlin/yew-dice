use crate::{context::provider::StateProvider, ui::PlayField};
#[allow(unused)]
use gloo_console::{debug, error, info};
use yew::prelude::*;
use yew_bootstrap::util::{include_cdn, include_cdn_js};

#[function_component]
pub fn App() -> Html {
    info!("Started");

    html! {
        <div>
            {include_cdn()}
            <StateProvider>
                <PlayField/>
            </StateProvider>
            {include_cdn_js()}
        </div>
    }
}
