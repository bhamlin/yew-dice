use crate::tools::generators::pick_name;
#[allow(unused)]
use gloo_console::{debug, error, info};
use std::fmt::Display;

#[derive(Debug, PartialEq)]
pub enum PlayerSide {
    Left,
    Right,
}

impl Default for PlayerSide {
    fn default() -> Self {
        Self::Left
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Player {
    pub name: String,
    pub score: i32,
    pub current_hand: Option<String>,
}

impl Default for Player {
    fn default() -> Self {
        Self::new(&pick_name())
    }
}

impl Player {
    pub fn new(name: &String) -> Self {
        Self {
            name: name.clone(),
            score: 0,
            current_hand: None,
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Players {
    pub left: Player,
    pub right: Player,
}

impl Default for Players {
    fn default() -> Self {
        let left_name = &pick_name();
        debug!("Left name:  ", left_name);
        let right_name = &pick_name();
        debug!("Right name: ", right_name);

        let left = Player::new(left_name);
        let right = Player::new(right_name);

        Self { left, right }
    }
}

impl Display for PlayerSide {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Left => "Left",
                Self::Right => "Right",
            }
        )
    }
}
