use yew::prelude::*;

use crate::context::state::{State, StateContext};

#[derive(Properties, Debug, PartialEq)]
pub struct StateProviderProps {
    #[prop_or_default]
    pub children: Children,
}

#[function_component]
pub fn StateProvider(props: &StateProviderProps) -> Html {
    let state = use_reducer(|| State::default());

    html! {
        <ContextProvider<StateContext> context={state}>
            {props.children.clone()}
        </ContextProvider<StateContext>>
    }
}
