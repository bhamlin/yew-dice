#[allow(unused)]
use gloo_console::{debug, error, info};
use std::{fmt::Display, rc::Rc};
use yew::prelude::*;

use super::model::Players;

pub type StateContext = UseReducerHandle<State>;

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum DisplayState {
    Empty,
}

impl Default for DisplayState {
    fn default() -> Self {
        Self::Empty
    }
}

#[allow(unused)]
pub enum StateAction {
    Nothing,
    Click,
}

impl Display for StateAction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            StateAction::Nothing => "Nothing",
            StateAction::Click => "Click",
        };

        write!(f, "{message}")
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct State {
    click_counter: u32,
    pub display_state: DisplayState,
    pub players: Players,
}

impl Default for State {
    fn default() -> Self {
        Self {
            click_counter: 0,
            display_state: DisplayState::Empty,
            players: Players::default(),
        }
    }
}

impl From<Rc<State>> for State {
    fn from(value: Rc<State>) -> Self {
        Self {
            click_counter: value.click_counter,
            display_state: value.display_state.clone(),
            players: value.players.clone(),
        }
    }
}

impl Reducible for State {
    type Action = StateAction;

    fn reduce(self: Rc<Self>, action: Self::Action) -> Rc<Self> {
        info!("Got action: {action}");
        let reduced_state = match action {
            StateAction::Click => {
                let mut new_state: Self = self.into();
                new_state.click_counter += 1;
                new_state.into()
            }
            StateAction::Nothing => self,
        };

        info!("Counter should be: {}", reduced_state.click_counter);

        reduced_state
    }
}

impl State {
    pub fn get_click_counter(&self) -> String {
        self.click_counter.to_string()
    }
}
