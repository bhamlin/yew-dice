mod app;
mod context;
mod tools;
mod ui;

use app::App;
#[allow(unused)]
use log::{debug, info};

fn main() {
    yew::Renderer::<App>::new().render();
}
