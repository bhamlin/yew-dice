use rand::seq::IteratorRandom;

const NAME_SOURCE: [&str; 30] = [
    "Genna Russ",
    "Rey Mantique",
    "Gal Gallant",
    "Melody Gale",
    "Faye Boulous",
    "Tina Scious",
    "Sofie Stication",
    "Bea Constrictor",
    "Pippa Pepper",
    "Sara Penth",
    "Lola Lavish",
    "Jue Wells",
    "Adda Miration",
    "Gena Rocity",
    "May Varlous",
    "Aura Ley",
    "Betty Brilliance",
    "Perry Fomance",
    "Satty Phection",
    "Sofie Stication",
    "Vall Iant",
    "Trixy Bunn",
    "Hella Cious",
    "Eva Siff",
    "Connie Fidence",
    "Raye Storm",
    "Super Nova",
    "Sara Castique",
    "Gal Galore",
    "Kaia Cayenne",
];

pub fn pick_name() -> String {
    NAME_SOURCE
        .iter()
        .choose(&mut rand::thread_rng())
        .unwrap_or(&NAME_SOURCE[0])
        .to_string()
}
