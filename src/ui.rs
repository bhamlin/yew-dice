mod counter;
mod elements;
mod player_control;
mod scoreboard;

use crate::{
    context::model::PlayerSide,
    ui::{
        // counter::{ClickButton, CounterDisplay},
        player_control::PlayerControl,
        scoreboard::Scoreboard,
    },
};
#[allow(unused)]
use gloo_console::{debug, error, info};
use yew::prelude::*;
use yew_bootstrap::component::{Column, Container, Row};

#[function_component]
pub fn Padding() -> Html {
    html! {
        <Row>
            <Column>
                {"\u{00a0}"}
            </Column>
        </Row>
    }
}

#[function_component]
pub fn HorizontalRuleLine() -> Html {
    html! {
        <Row>
            <Column>
                <hr/>
            </Column>
        </Row>
    }
}

#[function_component]
pub fn PlayField() -> Html {
    html! {
        <Container>
            <Padding/>
            <Row class="play-field">
                <Column>
                    <PlayerControl player_side={PlayerSide::Left}/>
                </Column>
                <Column>
                    <Scoreboard/>
                </Column>
                <Column>
                    <PlayerControl player_side={PlayerSide::Right}/>
                </Column>
            </Row>
        </Container>
    }
}
