use crate::context::state::{StateAction, StateContext};
use yew::prelude::*;

#[function_component]
pub fn ClickButton() -> Html {
    let state = use_context::<StateContext>().unwrap();
    let onclick = Callback::from(move |_| state.dispatch(StateAction::Click));

    html!(
        <button {onclick}>{ "+1" }</button>
    )
}

#[function_component]
pub fn CounterDisplay() -> Html {
    let state = use_context::<StateContext>().unwrap();
    let counter = state.get_click_counter();

    html!(
        <p>{ counter }</p>
    )
}
