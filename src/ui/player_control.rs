use crate::{
    context::{model::PlayerSide, state::StateContext},
    ui::HorizontalRuleLine,
};
#[allow(unused)]
use gloo_console::{debug, error, info};
use yew::prelude::*;
use yew_bootstrap::component::{Column, Container, Row};

#[derive(Properties, Debug, PartialEq)]
pub struct PlayerControlProps {
    #[prop_or_default]
    pub player_side: PlayerSide,
}

#[function_component]
pub fn PlayerControl(props: &PlayerControlProps) -> Html {
    let _state = use_context::<StateContext>().unwrap();
    let player = match props.player_side {
        PlayerSide::Left => &_state.players.left,
        PlayerSide::Right => &_state.players.right,
    };
    let name = &player.name.clone();
    let score_display = format!("Score: {}", player.score);

    html! {
        <Container class="player-board">
            <Row>
                <Column>
                    <div class="player-name">{name}</div>
                </Column>
            </Row>
            <HorizontalRuleLine/>
            <Row>
                <Column>
                    <div class="player-score">{score_display}</div>
                </Column>
            </Row>
        </Container>
    }
}
