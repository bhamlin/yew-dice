use crate::context::state::StateContext;
use yew::prelude::*;
use yew_bootstrap::component::{Column, Container, Row};

#[function_component]
pub fn Scoreboard() -> Html {
    let _state = use_context::<StateContext>().unwrap();

    html!(
        <Container class="score-board">
            <Row>
                <Column>
                    {"Scoreboard"}
                </Column>
            </Row>
        </Container>
    )
}
